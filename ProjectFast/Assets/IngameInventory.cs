﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class IngameInventory : MonoBehaviour
{
    public Dictionary<Button, IInventoryItem> SlotMap = new Dictionary<Button, IInventoryItem>();
    public List<Button> Slots;
    // Start is called before the first frame update
    void OnEnable()
    {
        var playerInven = FindObjectOfType<PlayerInventory>();
        var index = 0;
        Slots.ForEach(e => {
            try
            {
                var item = playerInven.Items[index];
                SlotMap[e] = item;
                Slots[index].GetComponent<ItemUI>().SetSprite(item.GetGameObject().GetComponent<SpriteRenderer>().sprite);
            }
            catch
            {
                SlotMap[e] = null;
                e.GetComponent<ItemUI>().Sprite = e.GetComponent<ItemUI>().Empty;
            }
            index++;
        });
        // Update is called once per frame
    }
}
