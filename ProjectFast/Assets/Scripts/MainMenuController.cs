﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public string sceneName;

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        GameplayState gameplayState = Store.MetaMap["GameplayState"] as GameplayState;
        RoomStates roomStates = Store.MetaMap["RoomState"] as RoomStates;
        gameplayState.shipHP = 100;

        foreach (var obs in roomStates.obstacleStatuses)
        {
            obs.obstacle = ObstacleType.None;
        }
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            SceneManager.LoadScene(sceneName);
        }
    }

    public void StartButton_cb()
    {
        Debug.Log("StartButton callbac");
        SceneManager.LoadScene(sceneName);
    }

    public void ExitButton_cb()
    {
        Debug.Log("ExitButton callback");
        Application.Quit();
    }
}
