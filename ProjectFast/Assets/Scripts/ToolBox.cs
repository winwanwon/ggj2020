﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolBox : MonoBehaviour, IInteractable
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Interact()
    {
        var UI = FindObjectOfType<UIManager>();
        UI.Switch("inv");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
