﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LiftState : int { OnUp, OnDown, GoingUp, GoingDown }

public class LiftController : MonoBehaviour
{
    [Range(0f,20f)] public int liftSpeed = 1;
    // reference
    public Rigidbody2D liftPlatformRigidbody;
    private Transform upSlotTransform;
    private Transform downSlotTransform;
    public LiftSlotController upSlot;
    public LiftSlotController downSlot;
    public PlayerTriggerController playerTrigger;
    public BoxCollider2D hiddenGround;


    public LiftState state;
    private void Awake()
    {
        state = LiftState.OnDown;
        Debug.Log("Awake");
    }

    private void Update()
    {
        
    }

    public void OnPushLiftButton()
    {
        var transforms = gameObject.GetComponentsInChildren<Transform>();
        foreach (var element in transforms)
        {
            if (element.gameObject.name == "UpSlot")
            {
                upSlotTransform = element;
            }
            if (element.gameObject.name == "DownSlot")
            {
                downSlotTransform = element;
            }
        }
        Rigidbody2D playerRb = FindObjectOfType<PlayerController>().rb;
        if (playerRb.position.y < upSlotTransform.position.y)
        {
            playerRb.position = new Vector2(playerRb.position.x, upSlotTransform.position.y + 1); // HACK
        }
        else
        {
            playerRb.position = new Vector2(playerRb.position.x, downSlotTransform.position.y + 1);
        }
        //switch (state)
        //{
            
        //    case LiftState.OnUp:
        //    case LiftState.GoingUp:
        //        state = LiftState.GoingDown;
                
        //        break;
        //    case LiftState.OnDown:
        //    case LiftState.GoingDown:
        //        state = LiftState.GoingUp; 
        //        break;
        //    default:
        //        break;
        //}
    }

    private void OnDestroy()
    {
        Debug.Log("Lift Ctrl Destroy");
        Destroy(this);
    }

    public void OnPlayerEnter()
    {

        hiddenGround.enabled = false;
    }
    public void OnPlayerExit()
    {

        hiddenGround.enabled = true;
    }

    
}
