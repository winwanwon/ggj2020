﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftSlotController : MonoBehaviour, IInteractable
{
    public AudioClip warpSound;
    private LiftSlotController liftSlotController;
    private GameObject Destination;
    public void Interact()
    {
        Debug.Log("Interact with Lift slot");
        Teleport();
        //liftController.OnPushLiftButton();
    }


    void Teleport()
    {
        Rigidbody2D playerRb = FindObjectOfType<PlayerController>().rb;
        if(this.gameObject.name == "DownSlot")
        {
            Destination = GameObject.Find("UpSlot");
        }
        else if(this.gameObject.name == "UpSlot")
        {
            Destination = GameObject.Find("DownSlot");
        }
        if (this.gameObject.name == "DownSlot2")
        {
            Destination = GameObject.Find("UpSlot2");
        }
        else if (this.gameObject.name == "UpSlot2")
        {
            Destination = GameObject.Find("DownSlot2");
        }
        playerRb.position = Destination.transform.position + Vector3.down;
        FindObjectOfType<SoundManager>().PlaySingle(warpSound);
    }
    
}
