﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObstacleController : MonoBehaviour, IInteractable
{
    public Sprite Fire;
    public Sprite Water;
    public Sprite Elect;
    public Sprite BlueFlame;
    public Sprite Poo;
    public Sprite GreenFlame;

    public AudioClip fixSound;
    public Fixer fixer;
    private RoomStates roomState;
    private ObstacleType obstacle;

    void Update()
    {
        // Check room state
        roomState = Store.MetaMap["RoomState"] as RoomStates;
        foreach (var obs in roomState.obstacleStatuses)
        {
            if (obs.sceneName == SceneManager.GetActiveScene().name)
            {
                this.obstacle = obs.obstacle;
            }
        }

        // Set sprite depends on obs status
        GetComponent<SpriteRenderer>().sprite = GetSprite();
        this.gameObject.name = GetObstacleName();
    }

    private Sprite GetSprite()
    {
        switch (obstacle)
        {
            case ObstacleType.Fire: return Fire;
            case ObstacleType.Water: return Water;
            case ObstacleType.Elect: return Elect;
            case ObstacleType.BlueFlame: return BlueFlame;
            case ObstacleType.GreenFlame: return GreenFlame;
            case ObstacleType.Poo: return Poo;
            default: return null;
        }
    }

    private string GetObstacleName()
    {
        switch (obstacle)
        {
            case ObstacleType.Fire: return "Fire";
            case ObstacleType.Water: return "Water";
            case ObstacleType.Elect: return "Electric";
            case ObstacleType.BlueFlame: return "BlueFlame";
            case ObstacleType.GreenFlame: return "GreenFlame";
            case ObstacleType.Poo: return "Poo";
            default: return "Obstacle";
        }
    }

    private void SetObstacleStatus(ObstacleType status)
    {
        foreach (var obs in roomState.obstacleStatuses)
        {
            if (obs.sceneName == SceneManager.GetActiveScene().name)
            {
              
                obs.obstacle = status;
            }
        }
    }

    public void Interact()
    {
        var player = FindObjectOfType<PlayerInventory>();
        var required = Store.MetaMap[gameObject.name] as FixerConfig;
        player.Items.ForEach(e =>
        {
            if(e.GetName() == required.Fixername)
            {
                StartCoroutine(Fix(e));
            }
        });
        
        
 
    }

    public IEnumerator Fix(IInventoryItem fixer)
    {
        FindObjectOfType<SoundManager>().PlaySingle(fixSound);
        yield return new WaitForSeconds(0.3f);
        fixer.DoStuffs();
        yield return new WaitForSeconds(1);

        SetObstacleStatus(ObstacleType.None);
        var fixerGO = fixer.GetGameObject();
        fixerGO.SetActive(false);
    }
}
