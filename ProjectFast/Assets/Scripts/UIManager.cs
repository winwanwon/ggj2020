﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{

    public bool IsOpen { get; set; }

    private PlayerController PlayerController;

    public GameObject Inventory;
    public GameObject MiniMap;
    public GameObject HP;
    public GameObject IngameInv;
    private static GameObject UIObject;

    public Sprite Fire;
    public Sprite Water;
    public Sprite Elect;
    public Sprite BlueFlame;
    public Sprite Poo;
    public Sprite GreenFlame;

    // Minimap Status
    public GameObject CanteenStatus;
    public GameObject RocketEngineStatus;
    public GameObject Oxygen1Status;
    public GameObject Oxygen2Status;
    public GameObject BossStatus;
    public GameObject SightSeeingStatus;
    public GameObject CockpitStatus;
    public GameObject TcsStatus;
    public GameObject RooftopStatus;
    public GameObject EngineStatus;
    public GameObject LibraryStatus;

    private RoomStates roomStates;
    private GameplayState gameplayState;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if (UIObject == null)
        {
            UIObject = this.gameObject;
        }
        else
        {
            Object.Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
        var player = FindObjectOfType<PlayerController>();
        PlayerController = player;

        gameplayState = Store.MetaMap["GameplayState"] as GameplayState;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateMiniMapStatus();
        UpdateHPBar();
        IsOpen = IsActive();

    }

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if(scene.name == "StartScene" || scene.name == "GameOver")
        {
            HP.SetActive(false);
        } else
        {
            HP.SetActive(true);
        }
    }

    bool IsActive()
    {
        return Inventory.activeSelf || MiniMap.activeSelf || IngameInv.activeSelf;
    }

    public void Switch(string str)
    {
        if (IsOpen)
        {
             Close();
  
        }
        else
        {
            Open(str);
         
        }

    }

    public void Close()
    {
        
        MiniMap.SetActive(false);
        Inventory.SetActive(false);
        IngameInv.SetActive(false);
    }

    public void Open(string str)
    {
        if (str == "mmp")
        {
            
            MiniMap.SetActive(true);
        }
        if(str == "inv")
        {
            Inventory.SetActive(true);
        }
        if(str == "iginv")
        {
            IngameInv.SetActive(true);
        }
    }

    private void UpdateMiniMapStatus()
    {
        roomStates = Store.MetaMap["RoomState"] as RoomStates;
        foreach (var obs in roomStates.obstacleStatuses)
        {
            if(obs.sceneName == "Oxygen1")
            {
                if(GetSprite(obs.obstacle) != null)
                {
                    Oxygen1Status.SetActive(true);
                    Oxygen1Status.GetComponent<Image>().sprite = GetSprite(obs.obstacle);
                } else
                {
                    Oxygen1Status.SetActive(false);
                }
            }
            if (obs.sceneName == "Oxygen2")
            {
                if (GetSprite(obs.obstacle) != null)
                {
                    Oxygen2Status.SetActive(true);
                    Oxygen2Status.GetComponent<Image>().sprite = GetSprite(obs.obstacle);
                } else
                {
                    Oxygen2Status.SetActive(false);
                }
            }
            if (obs.sceneName == "Engine1")
            {
                if (GetSprite(obs.obstacle) != null)
                {
                    EngineStatus.SetActive(true);
                    EngineStatus.GetComponent<Image>().sprite = GetSprite(obs.obstacle);
                } else
                {
                    EngineStatus.SetActive(false);
                }
            }
            if (obs.sceneName == "EngineLarge1")
            {
                if (GetSprite(obs.obstacle) != null)
                {
                    RocketEngineStatus.SetActive(true);
                    RocketEngineStatus.GetComponent<Image>().sprite = GetSprite(obs.obstacle);
                } else
                {
                    RocketEngineStatus.SetActive(false);
                }
            }
            if (obs.sceneName == "Canteen1")
            {
                if (GetSprite(obs.obstacle) != null)
                {
                    CanteenStatus.SetActive(true);
                    CanteenStatus.GetComponent<Image>().sprite = GetSprite(obs.obstacle);
                } else
                {
                    CanteenStatus.SetActive(false);
                }
            }
            if (obs.sceneName == "Rooftop1")
            {
                if (GetSprite(obs.obstacle) != null)
                {
                    RooftopStatus.SetActive(true);
                    RooftopStatus.GetComponent<Image>().sprite = GetSprite(obs.obstacle);
                } else
                {
                    RooftopStatus.SetActive(false);
                }
            }
            if (obs.sceneName == "Library1")
            {
                if (GetSprite(obs.obstacle) != null)
                {
                    LibraryStatus.SetActive(true);
                    LibraryStatus.GetComponent<Image>().sprite = GetSprite(obs.obstacle);
                } else
                {
                    LibraryStatus.SetActive(false);
                }
            }
            if (obs.sceneName == "BossRoom1")
            {
                if (GetSprite(obs.obstacle) != null)
                {
                    BossStatus.SetActive(true);
                    BossStatus.GetComponent<Image>().sprite = GetSprite(obs.obstacle);
                } else
                {
                    BossStatus.SetActive(false);
                }
            }
            if (obs.sceneName == "Sightsee1")
            {
                if (GetSprite(obs.obstacle) != null)
                {
                    SightSeeingStatus.SetActive(true);
                    SightSeeingStatus.GetComponent<Image>().sprite = GetSprite(obs.obstacle);
                } else
                {
                    SightSeeingStatus.SetActive(false);
                }
            }
            if (obs.sceneName == "Cockpit1")
            {
                if (GetSprite(obs.obstacle) != null)
                {
                    CockpitStatus.SetActive(true);
                    CockpitStatus.GetComponent<Image>().sprite = GetSprite(obs.obstacle);
                } else
                {
                    CockpitStatus.SetActive(false);
                }
            }
            if (obs.sceneName == "Tcs1")
            {
                if (GetSprite(obs.obstacle) != null)
                {
                    TcsStatus.SetActive(true);
                    TcsStatus.GetComponent<Image>().sprite = GetSprite(obs.obstacle);
                } else
                {
                    TcsStatus.SetActive(false);
                }
            }
        }
    }

    private Sprite GetSprite(ObstacleType obstacle)
    {
        switch (obstacle)
        {
            case ObstacleType.Fire: return Fire;
            case ObstacleType.Water: return Water;
            case ObstacleType.Elect: return Elect;
            case ObstacleType.BlueFlame: return BlueFlame;
            case ObstacleType.GreenFlame: return GreenFlame;
            case ObstacleType.Poo: return Poo;
            default: return null;
        }
    }

    private void UpdateHPBar()
    {
        float hpPercent = (gameplayState.shipHP / 100f);
        HP.GetComponent<Slider>().value = hpPercent;
    }
}
