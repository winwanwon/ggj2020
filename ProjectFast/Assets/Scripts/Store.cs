﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ObstacleType
{
    None,
    Fire,
    Water,
    Elect,
    BlueFlame,
    Poo,
    GreenFlame
}



public static class Store
{
    // Start is called before the first frame update
    public static Dictionary<string, Object> MetaMap = new Dictionary<string, Object>()
    {
        { "Fire", new FixerConfig("FireFixer")  },
        { "Water", new FixerConfig("WaterFixer") },
        { "Electric", new FixerConfig("ElectricFixer")},
        { "BlueFlame", new FixerConfig("BlueFlameFixer")  },
        { "Poo", new FixerConfig("PooFixer") },
        { "GreenFlame", new FixerConfig("GreenFlameFixer")},
        { "RoomState", new RoomStates() },
        { "GameplayState", new GameplayState() },
        { "Obstacle", null }
    };

}

public class FixerConfig : Object
{
    public string Fixername;
    public FixerConfig(string inp)
    {
        Fixername = inp;
    }
}

public class GameplayState : Object
{
    public int shipHP = 100;
    public int score = 0;
}

public class ObstacleStatus
{
    public string sceneName;
    public ObstacleType obstacle;
}

public class RoomStates : Object
{
    public List<ObstacleStatus> obstacleStatuses = new List<ObstacleStatus>
    {
        new ObstacleStatus { sceneName = "Oxygen1", obstacle = ObstacleType.None },
        new ObstacleStatus { sceneName = "Oxygen2", obstacle = ObstacleType.None },
        new ObstacleStatus { sceneName = "Engine1", obstacle = ObstacleType.None },
        new ObstacleStatus { sceneName = "EngineLarge1", obstacle = ObstacleType.None },
        new ObstacleStatus { sceneName = "Canteen1", obstacle = ObstacleType.None },
        new ObstacleStatus { sceneName = "Rooftop1", obstacle = ObstacleType.None },
        new ObstacleStatus { sceneName = "Library1", obstacle = ObstacleType.None },
        new ObstacleStatus { sceneName = "BossRoom1", obstacle = ObstacleType.None },
        new ObstacleStatus { sceneName = "Sightsee1", obstacle = ObstacleType.None },
        new ObstacleStatus { sceneName = "Cockpit1", obstacle = ObstacleType.None },
        new ObstacleStatus { sceneName = "Tcs1", obstacle = ObstacleType.None },
        // === SAFE ROOMS ===
        //{ "Room1", ObstacleType.None },
        //{ "ToolboxRoom1", ObstacleType.None },
        //{ "ToolboxRoom2", ObstacleType.None },
        //new ObstacleStatus { sceneName = "Hallway1", obstacle = ObstacleType.None },
        //new ObstacleStatus { sceneName = "Hallway2", obstacle = ObstacleType.None },
    };
}


