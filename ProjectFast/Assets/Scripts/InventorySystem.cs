﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySystem : MonoBehaviour
{

    public Dictionary<Button, IInventoryItem> SlotMap = new Dictionary<Button, IInventoryItem>();
    public List<Button> Slots;
    public Dictionary<Button, IInventoryItem> ToolsBoxMap = new Dictionary<Button, IInventoryItem>();
    public List<Button> ToolsBox;

    private PlayerInventory PlayerInventory;
    private int currentTBIndex;
    private int lastTBIndex = 1;
    private int currentItemIndex;
    private int lastItemIndex = 1;

    private bool moveInuse = false;
    private bool isToolTab = false;

    
    // Start is called before the first frame update
    void Awake()
    {
        isToolTab = false;
        var playerInven = FindObjectOfType<PlayerInventory>();
        PlayerInventory = playerInven;
        

        currentTBIndex = 0;
        currentItemIndex = 0;

    }

    // Update is called once per frame
    void Update()
    {
        Sync();
        SelectToolBox(currentTBIndex);
        //SelectItem(currentItemIndex);
        var axis = Input.GetAxisRaw("Horizontal");
        if (axis > 0 && !moveInuse)
        {
            if(!isToolTab)
            {
                lastTBIndex = currentTBIndex;
                moveInuse = true;
                if (currentTBIndex + 1 >= ToolsBox.Count)
                {
                    currentTBIndex = 0;
                }
                else
                    currentTBIndex += 1;
            }
            else
            {
                
                lastItemIndex = currentItemIndex;
                moveInuse = true;
                if (currentItemIndex + 1 >= Slots.Count)
                {
                    currentItemIndex = 0;
                }
                else
                    currentItemIndex += 1;
            }
            
        }
        if (axis < 0 && !moveInuse)
        {
            if (!isToolTab)
            {
                lastTBIndex = currentTBIndex;
                moveInuse = true;
                if (currentTBIndex - 1 < 0)
                {
                    currentTBIndex = ToolsBox.Count - 1;
                }
                else
                    currentTBIndex -= 1;
            }
            else
            {
                lastItemIndex = currentItemIndex;
                moveInuse = true;
                if (currentItemIndex - 1 < 0)
                {
                    currentItemIndex = Slots.Count - 1;
                }
                else
                    currentItemIndex -= 1;
            }
            
        }
        if(axis == 0)
        {
            moveInuse = false;
        }

        if(Input.GetButtonDown("Accept"))
        {
            //isToolTab = !isToolTab;
            //print("Accept");
            MoveIn();
        }

    }


    Button SelectToolBox(int index)
    {
        
        ToolsBox[index].GetComponent<ItemUI>().Select(new Color(255,0,0));
        ToolsBox[lastTBIndex].GetComponent<ItemUI>().DeSelect();
        return ToolsBox[index];
    }

    Button SelectItem(int index)
    {

        Slots[index].GetComponent<ItemUI>().Select(new Color(0, 255, 0));
        Slots[lastItemIndex].GetComponent<ItemUI>().DeSelect();
        return ToolsBox[index];
    }

    void MoveIn()
    {
        PlayerInventory.Add(ToolsBoxMap[ToolsBox[currentTBIndex]]);
        ToolsBox[currentTBIndex].GetComponent<ItemUI>().Sprite = ToolsBox[currentTBIndex].GetComponent<ItemUI>().Empty;
        Sync();
    }

    private void Sync()
    {
        var index = 0;
        ToolsBox.ForEach(e => {
            try
            {
                var item = PlayerInventory.ToolBoxItem[index];
                ToolsBoxMap[e] = item;
                ToolsBox[index].GetComponent<ItemUI>().SetSprite(item.GetGameObject().GetComponent<SpriteRenderer>().sprite);
            }
            catch
            {
                ToolsBoxMap[e] = null;
                e.GetComponent<ItemUI>().Sprite = e.GetComponent<ItemUI>().Empty;
            }

            index++;
        });
        index = 0;
        Slots.ForEach(e => {
            try
            {
                var item = PlayerInventory.Items[index];
                SlotMap[e] = item;
                Slots[index].GetComponent<ItemUI>().SetSprite(item.GetGameObject().GetComponent<SpriteRenderer>().sprite);
            }
            catch
            {
                SlotMap[e] =  null;
                e.GetComponent<ItemUI>().Sprite = e.GetComponent<ItemUI>().Empty;
            }
            index++;
        });
    }
    

    int GetIndex(Button btn)
    {
        return int.Parse(btn.name.Substring(btn.name.Length - 1)) - 1;
    }

}
