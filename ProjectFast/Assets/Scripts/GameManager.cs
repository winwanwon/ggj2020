﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public float obstacleGenerateInterval = 1.0f;
    public float damageInterval = 1.0f;
    public float scoreInterval = 1.0f;
    private static RoomStates roomState;
    private static GameObject gameManagerObject;
    private static GameplayState gameplayState;
    private int roundCount = 1;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if (gameManagerObject == null)
        {
            gameManagerObject = this.gameObject;
        }
        else
        {
            Object.Destroy(gameObject);
        }

        if(SceneManager.GetActiveScene().name == "StartScene")
        {
            Object.Destroy(gameObject);
        }
    }

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Hallway1")
        {
            if (!IsInvoking("GenerateObstacle"))
            {
                InvokeRepeating("GenerateObstacle", 0f, obstacleGenerateInterval);
            }
            if (!IsInvoking("ShipDamageUpdate"))
            {
                InvokeRepeating("ShipDamageUpdate", 0f, damageInterval);
            }
            if (!IsInvoking("ScoreUpdate"))
            {
                InvokeRepeating("ScoreUpdate", 0f, scoreInterval);
            }
        }
    }

    void Start()
    {
        roomState = Store.MetaMap["RoomState"] as RoomStates;
        gameplayState = Store.MetaMap["GameplayState"] as GameplayState;
        //InvokeRepeating("GenerateObstacle", 0f, obstacleGenerateInterval);
        //InvokeRepeating("ShipDamageUpdate", 0f, damageInterval);
        //InvokeRepeating("ScoreUpdate", 0f, scoreInterval);
        gameplayState.shipHP = 100;
    }

    private ObstacleType RandomObstacleType()
    {
        var values = ObstacleType.GetValues(typeof(ObstacleType));
        return (ObstacleType) values.GetValue(Random.Range(1, values.Length));
    }

    private int RandomRoomIndex()
    {
        return Random.Range(0, roomState.obstacleStatuses.Count);
    }

    private bool ShouldGenerateObs(int roundCount)
    {
        Debug.Log("roundCount: " + roundCount);
        if(roundCount <= 30)
        {
            return roundCount % 10 == 0; // every 10s
        } else if (roundCount <= 100)
        {
            return roundCount % 7 == 0; // every 7s
        } else
        {
            return roundCount % 5 == 0; // every 5s
        }
    }

    private void GenerateObstacle()
    {
        if (!FindObjectOfType<UIManager>().IsOpen)
        {
            roundCount += 1;

            if (ShouldGenerateObs(roundCount))
            {
                // Set obstacle status to store
                var randomRoomIndex = RandomRoomIndex();
                int roomLoopCount = 0;
                while (roomState.obstacleStatuses[randomRoomIndex].obstacle != ObstacleType.None)
                {
                    randomRoomIndex = RandomRoomIndex();
                    roomLoopCount += 1;
                    if (roomLoopCount == roomState.obstacleStatuses.Count) break;
                }
                roomState.obstacleStatuses[randomRoomIndex].obstacle = RandomObstacleType();
                Debug.Log("Room: " + roomState.obstacleStatuses[randomRoomIndex].sceneName + " => " + roomState.obstacleStatuses[randomRoomIndex].obstacle);
            }
        }
    }

    private int ObsCount()
    {
        int obsCount = 0;
        // Check if there's any obstacle
        foreach (var obs in roomState.obstacleStatuses)
        {
           if(obs.obstacle != ObstacleType.None)
            {
                obsCount += 1;
            }
        }
        return obsCount;
    }

    private int CalculateShipDamage(int obsCount)
    {
        Debug.Log(obsCount);
        switch (obsCount)
        {
            case 0: return 0; 
            case 1: return 1;
            case 2: return 1;
            case 3: return 2;
            case 4: return 2;
            case 5: return 2;
            case 6: return 2;
            case 7: return 3;
            case 8: return 3;
            case 9: return 5;
            case 10: return 5;
            case 11: return 8;
            default: return 0;
        }
    }

    private void ShipDamageUpdate()
    {
        if (!FindObjectOfType<UIManager>().IsOpen)
        {
            var obsCount = ObsCount();
            if (obsCount > 0)
            {
                gameplayState.shipHP -= CalculateShipDamage(obsCount);
            }

            if (gameplayState.shipHP <= 0)
            {
                // GameOver
                CancelInvoke();
                SceneManager.LoadScene("GameOver");
                roundCount = 1;
            }

            Debug.Log("Ship HP: " + gameplayState.shipHP);
        }
    }

    private void ScoreUpdate()
    {
        if (!FindObjectOfType<UIManager>().IsOpen)
        {
            if (gameplayState.shipHP > 0)
            {
                gameplayState.score += 50;
            }
        }
    }
}
