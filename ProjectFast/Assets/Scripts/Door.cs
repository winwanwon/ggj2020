﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour
{
    public int doorNum;
    private string destScene;
    private List<string> keyList = new List<string> { };

    private void Start()
    {
        keyList = DoorState.GetRequiredKeyList(doorNum, SceneManager.GetActiveScene().name);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(keyList.Count == 0)
        {
            if (collision.gameObject.CompareTag("Player") && gameObject.tag == "Untagged") SceneChange();
            if (collision.gameObject.CompareTag("Player") && gameObject.tag == "UnlockedDoor" && Input.GetButtonDown("Jump")) SceneChange();
        } else
        {
            // Check if there's a key item in Player inventory
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && gameObject.tag == "UnlockedDoor" && Input.GetButtonDown("Jump")) SceneChange();
    }

    private void SceneChange()
    {
        destScene = LevelNavigationHelper.GetSceneDest(SceneManager.GetActiveScene().name, doorNum);
        SceneManager.LoadScene(destScene);
    }
}
