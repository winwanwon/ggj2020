﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{

    public List<IInventoryItem> Items = new List<IInventoryItem>();
    public List<IInventoryItem> ToolBoxItem = new List<IInventoryItem>();
    // Start is called before the first frame update
    void Awake()
    {
        var fixers = FindObjectsOfType<Fixer>();
        foreach(var e in fixers)
        {
            ToolBoxItem.Add(e);
            e.gameObject.SetActive(false);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Pop()
    {
        ToolBoxItem.Add(Items[0]);
        Items.RemoveAt(0);
    }
    public void Add(IInventoryItem itemIn)
    {
        if(itemIn == null)
        {
            Pop();
            return;
        }
        ToolBoxItem.Remove(itemIn);
        if(Items.Count >= 3)
        {
            Pop();
            Items.Add(itemIn);
        }
        else
        {
          Items.Add(itemIn);
        }
    }

    private void Switch(IInventoryItem itemIn)
    {

    }

    public void Remove(IInventoryItem itemOut)
    {
        ToolBoxItem.Add(itemOut);
        Items.Remove(itemOut);
     
    }

    public void Clear()
    {
        Items.ForEach(e => {

            ToolBoxItem.Add(e);
        }
        );

        Items.Clear();


    }

}
