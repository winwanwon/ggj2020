﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fixer : MonoBehaviour, IInventoryItem
{

    public string GetName()
    {
        return this.gameObject.name;
    }

    public void DoStuffs()
    {

        var playerController = FindObjectOfType<PlayerController>();
        playerController.StartFixing();
        this.gameObject.SetActive(true);
        print("Fixer: start fixing");
    }

    public GameObject GetGameObject()
    {
        return this.gameObject;
    }

}
