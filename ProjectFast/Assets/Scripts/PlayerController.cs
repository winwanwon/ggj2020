﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    public AudioClip walkSound;
    public SpriteRenderer ext;
    // component reference
    [HideInInspector] public Rigidbody2D rb;
    private SpriteRenderer spriteRenderer;
    private CircleCollider2D circleCollider2D;
    private UIManager UI;
    private static GameObject playerInstance;
    public bool CanMove { get; set; }
    public Animator Animator;

    private bool inventoryButtonInUse;
    private bool isFixing = false;

    // variable
    [Range(0f, 10f)] public float fallSpeed;
    [Range(0f, 10f)] public float hSpeed;
    [Range(0f, 1f)] public float hAccelerate;
    [Range(0f, 1f)] public float hBreak;
    private float move;

    // interact
    private List<IInteractable> interactingObjects = new List<IInteractable>();

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        circleCollider2D = GetComponent<CircleCollider2D>();
        UI = FindObjectOfType<UIManager>();
        DontDestroyOnLoad(gameObject);
        if (playerInstance == null) 
        {
            playerInstance = this.gameObject;
        } else {
            Object.Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
     
        CanMove = !UI.IsOpen;

        if (!isFixing)
        {
            if (Input.GetButtonDown("Jump"))
            {
                
                interactingObjects.RemoveAll(e => e.Equals(null));
                if (interactingObjects.Count > 0)
                {
                    
                    interactingObjects[0].Interact();
                }
            }
            if (CanMove)
            {
                move = Input.GetAxisRaw("Horizontal");
            }
        }

        if (Input.GetButtonDown("Pause"))
        {
            print("Map");
            UI.Switch("mmp");
        }
        if(Animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            isFixing = false;
        }
        if(Input.GetButtonDown("IGINV"))
        {
            UI.Switch("iginv");
        }
    }

    void FixedUpdate()
    {
        if(CanMove && !isFixing)
        {
            Move();
        }
        
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        var interactingObject = collider.gameObject.GetComponent(typeof(IInteractable)) as IInteractable;
        if (interactingObject != null && !interactingObject.Equals(null))
        {
            interactingObjects.Add(interactingObject);

            // ext
            if (collider.gameObject.name != "Obstacle")
            {
                ext.enabled = true;
            }

        }
        // if (interactingObject)
        // ext.enabled = (interactingObjects.Count != 0);
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        interactingObjects.Remove(collider.gameObject.GetComponent(typeof(IInteractable)) as IInteractable);
        ext.enabled = false;
    }

    void OnSceneLoaded()
    {
        ext.enabled = false;
    }

    void OnDisable()
    {
        // SceneManager.sceneLoaded -= OnSceneLoaded;    
    }

    void Move()
    {   
        float velX, velY; //target horizontal velocity
        velX = rb.velocity.x;
        velY = rb.velocity.y;
        // stupidly check ground
        if (Mathf.Approximately(rb.velocity.y, 0) && CanMove)
        {
            if (move != 0)
            {
                if (move > 0) 
                {
                    spriteRenderer.flipX = false;
                } 
                else
                {
                    spriteRenderer.flipX = true;
                }
                // accerelate to hSpeed
                velX = Mathf.Lerp(rb.velocity.x, move * hSpeed, hAccelerate);
            }
            else
            {
                // break to zero
                velX = Mathf.Lerp(rb.velocity.x, 0, hBreak);
            }
        }

        // limit at terminal velocity
        // else if (rb.velocity.y < - fallSpeed)
        // {
        //     velY = - fallSpeed;
        // }
        
        // set velocity
        rb.velocity = new Vector2(velX, velY);
        Animator.SetFloat("Speed",move);
    }

    public void StartFixing()
    {
        isFixing = true;
        Animator.SetBool("IsFixing", isFixing);
    }

    public void EndFixing()
    {
        print("End");
        isFixing = false;
        Animator.SetBool("IsFixing", isFixing);
    }

    public void PlayWalkSound()
    {
        FindObjectOfType<SoundManager>().PlaySingle(walkSound);
    }

    
    
}
