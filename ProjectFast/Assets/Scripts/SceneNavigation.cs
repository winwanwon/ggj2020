﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class SceneNavigation : MonoBehaviour
{

    public GameObject doorObject;

    public Tilemap doorTileMap;
    public Tilemap triggerableDoorTileMap = null;
    public Tilemap spawnPointTileMap;
    private GameObject player;
    private static int doorNum = 0;

    void Start()
    {
        doorNum = 0;
        player = GameObject.FindGameObjectsWithTag("Player")[0];

        GenerateDoorObject(doorTileMap);
        if (triggerableDoorTileMap) GenerateDoorObject(triggerableDoorTileMap, true);

        if (LevelNavigationHelper.PlayerLatestDoorDest != null)
        {
            MovePlayerToSpawnPoint(player, spawnPointTileMap);
        }
    }

    private void MovePlayerToSpawnPoint(GameObject player, Tilemap tilemap)
    {
        player.gameObject.transform.position = LevelNavigationHelper.FindSpawnPosition(Int32.Parse(LevelNavigationHelper.PlayerLatestDoorDest), tilemap);
    }

    private void GenerateDoorObject(Tilemap doorTileMap, bool isLockable = false, bool lockState = false)
    {
        foreach (var doorPosition in doorTileMap.cellBounds.allPositionsWithin)
        {
            if (doorTileMap.GetTile(doorPosition))
            {
                GameObject door = Instantiate(doorObject, new Vector3(doorPosition.x, doorPosition.y, doorPosition.z), Quaternion.identity);
                if (isLockable)
                {
                    if (lockState) {
                        door.tag = "LockedDoor";
                    } else {
                        door.tag = "UnlockedDoor";
                    }
                };
                door.GetComponent<Door>().doorNum = doorNum;
                doorNum += 1;
            }
        }
    }
}
