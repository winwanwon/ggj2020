﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DoorState
{
    public static readonly Dictionary<string, List<string>> Room1 = new Dictionary<string, List<string>>()
    {
        { "0", new List<string>{ } }
    };

    public static readonly Dictionary<string, List<string>> Hallway1 = new Dictionary<string, List<string>>()
    {
        { "0", new List<string>{ } }
    };

    private static Dictionary<string, List<string>> MapSceneToDoorState(string scene)
    {
        switch (scene)
        {
            case "Room1": return Room1;
            case "Hallway1": return Hallway1;
            default: return Room1;
        }
    }

    public static List<string> GetRequiredKeyList(int doorNum, string sceneName)
    {
        Dictionary<string, List<string>> doorStateDict = MapSceneToDoorState(sceneName);
        if (doorStateDict.ContainsKey(doorNum.ToString()))
        {
            return doorStateDict[doorNum.ToString()] ?? new List<string> { };
        }
        return new List<string> { };
    }
}
