﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public static class LevelNavigationHelper
{

    public static string PlayerLatestDoorDest;

    public static readonly Dictionary<string, Dictionary<string, string>> Room1 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "Hallway1",
                ["Door"] = "1",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "Hallway1",
                ["Door"] = "1",
            }
        },
        { "2",
            new Dictionary<string, string>
            {
                ["Scene"] = "Hallway1",
                ["Door"] = "1",
            }
        },
        { "3",
            new Dictionary<string, string>
            {
                ["Scene"] = "Hallway1",
                ["Door"] = "1",
            }
        },
    };

    public static readonly Dictionary<string, Dictionary<string, string>> Hallway1 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "Room1",
                ["Door"] = "0",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "Oxygen1",
                ["Door"] = "1",
            }
        },
        { "2",
            new Dictionary<string, string>
            {   
                ["Scene"] = "Engine1",
                ["Door"] = "0",
            }
        },
        { "3",
            new Dictionary<string, string>
            {
                ["Scene"] = "Canteen1",
                ["Door"] = "1",
            }
        },
        { "4",
            new Dictionary<string, string>
            {
                ["Scene"] = "BossRoom1",
                ["Door"] = "0",
            }
        },
    };

    public static readonly Dictionary<string, Dictionary<string, string>> Oxygen1 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "ToolboxRoom1",
                ["Door"] = "1",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "Hallway1",
                ["Door"] = "0",
            }
        },
    };

    public static readonly Dictionary<string, Dictionary<string, string>> Oxygen2 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "Engine1",
                ["Door"] = "1",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "Sightsee1",
                ["Door"] = "0",
            }
        },
    };

    public static readonly Dictionary<string, Dictionary<string, string>> Engine1 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "Hallway1",
                ["Door"] = "2",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "Oxygen2",
                ["Door"] = "0",
            }
        },
    };

    public static readonly Dictionary<string, Dictionary<string, string>> Toolbox1 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "EngineLarge1",
                ["Door"] = "0",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "Oxygen1",
                ["Door"] = "0",
            }
        },
    };

    public static readonly Dictionary<string, Dictionary<string, string>> EngineLarge1 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "ToolboxRoom1",
                ["Door"] = "0",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "Canteen1", 
                ["Door"] = "0",
            }
        },
    };

    public static readonly Dictionary<string, Dictionary<string, string>> Canteen1 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "EngineLarge1",
                ["Door"] = "1",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "Hallway1", 
                ["Door"] = "3",
            }
        },
        { "2",
            new Dictionary<string, string>
            {
                ["Scene"] = "Rooftop1",
                ["Door"] = "0",
            }
        },
    };

    public static readonly Dictionary<string, Dictionary<string, string>> Rooftop1 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "Canteen1",
                ["Door"] = "2",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "Library1",
                ["Door"] = "0",
            }
        },
    };

    public static readonly Dictionary<string, Dictionary<string, string>> Library1 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "Rooftop1",
                ["Door"] = "1",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "Tcs1",
                ["Door"] = "0",
            }
        },
    };

    public static readonly Dictionary<string, Dictionary<string, string>> BossRoom1 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "Hallway1",
                ["Door"] = "4",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "ToolboxRoom2",
                ["Door"] = "0",
            }
        },
    };

    public static readonly Dictionary<string, Dictionary<string, string>> Sightsee1 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "Oxygen2",
                ["Door"] = "1",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "Hallway2", // Need Update
                ["Door"] = "1",
            }
        },
    };

    public static readonly Dictionary<string, Dictionary<string, string>> Tcs1 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "Hallway2",
                ["Door"] = "2",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "Library1",
                ["Door"] = "1",
            }
        },
    };

    public static readonly Dictionary<string, Dictionary<string, string>> Toolbox2 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "BossRoom1",
                ["Door"] = "1",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "Hallway2",
                ["Door"] = "0",
            }
        },
    };


    public static readonly Dictionary<string, Dictionary<string, string>> Hallway2 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "Sightsee1",
                ["Door"] = "1",
            }
        },
        { "1",
            new Dictionary<string, string>
            {
                ["Scene"] = "ToolboxRoom2",
                ["Door"] = "1",
            }
        },
        { "2",
            new Dictionary<string, string>
            {
                ["Scene"] = "Cockpit1",
                ["Door"] = "0",
            }
        },
        { "3",
            new Dictionary<string, string>
            {
                ["Scene"] = "Tcs1",
                ["Door"] = "1",
            }
        },
    };

    public static readonly Dictionary<string, Dictionary<string, string>> Cockpit1 = new Dictionary<string, Dictionary<string, string>>()
    {
        { "0",
            new Dictionary<string, string>
            {
                ["Scene"] = "Hallway2",
                ["Door"] = "3",
            }
        },
    };

    private static Dictionary<string, Dictionary<string, string>> MapSceneToDict(string scene)
    {
        switch (scene)
        {
            case "Room1": return Room1;
            case "Hallway1": return Hallway1;
            case "Oxygen1": return Oxygen1;
            case "Oxygen2": return Oxygen2;
            case "Engine1": return Engine1;
            case "ToolboxRoom1": return Toolbox1;
            case "ToolboxRoom2": return Toolbox2;
            case "EngineLarge1": return EngineLarge1;
            case "Canteen1": return Canteen1;
            case "Rooftop1": return Rooftop1;
            case "Library1": return Library1;
            case "BossRoom1": return BossRoom1;
            case "Sightsee1": return Sightsee1;
            case "Hallway2": return Hallway2;
            case "Tcs1": return Tcs1;
            case "Cockpit1": return Cockpit1;
            default: return Hallway1;
        }
    }

    public static string GetSceneDest(string scene, int door)
    {
        var SceneMapObject = MapSceneToDict(scene);

        if (SceneMapObject.ContainsKey(door.ToString()))
        {
            PlayerLatestDoorDest = SceneMapObject[door.ToString()]["Door"];
            return SceneMapObject[door.ToString()]["Scene"];
        }

        return string.Empty;
    }

    public static Vector3 FindSpawnPosition(int doorNum, Tilemap tilemap)
    {
        int currentDoorNum = 0;
        foreach (var tilePos in tilemap.cellBounds.allPositionsWithin)
        {
            // If the tile is valid
            if (tilemap.GetTile(tilePos))
            {
                if (currentDoorNum == doorNum) {
                    return new Vector3(tilePos.x, tilePos.y, tilePos.z);
                } else {
                    currentDoorNum += 1;
                };
            }
        }

        return new Vector3(0, 0, 0);
    }
};
