﻿using System;
using UnityEngine;

public interface IInventoryItem 
{
    void DoStuffs();
    string GetName();
    GameObject GetGameObject();
    
}
