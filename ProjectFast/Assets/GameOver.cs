﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameOver : MonoBehaviour
{

    public Text scoreText;

    private GameplayState gameplayState;

    // Start is called before the first frame update
    void Start()
    {
        gameplayState = Store.MetaMap["GameplayState"] as GameplayState;
        scoreText.text = "SCORE: " + gameplayState.score;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            ToMainMenu();
        }
    }

    public void ToMainMenu()
    {
        SceneManager.LoadScene("StartScene");
    }
}
